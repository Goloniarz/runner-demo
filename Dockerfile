FROM python:3.10.12

WORKDIR /app

COPY . /app

RUN pip install Flask

ENV NAME=Mark

CMD ["python", "app.py"]
